import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherService } from '../services/weather.service';

@Component({
  selector: 'app-weather-details',
  templateUrl: './weather-details.component.html',
  styleUrls: ['./weather-details.component.scss']
})
export class WeatherDetailsComponent implements OnInit {
  weatherForecasts = [];
  city = ''

  constructor(
    private activatedRoute: ActivatedRoute,
    private weatherService: WeatherService,
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((parameter) => {
      if (parameter && parameter.city) {
        this.city = parameter.city;
        this.getDetailsfor5days(parameter.city)
      }
    })
  }

  getDetailsfor5days(city) {
    this.weatherService.get5DaysReport(city, 5).subscribe((res: any) => {
      console.log(res);
      this.weatherForecasts = res.list
    })
  }

}
