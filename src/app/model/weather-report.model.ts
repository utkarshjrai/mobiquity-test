export class WeatherInfo {
	name: string;
	temp: any;
	sunrise: any;
  sunset: any;
  timezone: any;

	constructor(data) {
    this.name = data && data.name ? data.name : '';
		this.temp = data && data.main && data.main.temp ? data.main.temp : 0;
		this.sunrise = data && data.sys && data.sys.sunrise ? data.sys.sunrise : 0;
    this.sunset = data && data.sys && data.sys.sunset ? data.sys.sunset : 0;
    this.timezone = data && data.timezone ? data.timezone : 0;
	}
}
