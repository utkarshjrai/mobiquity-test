import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../services/weather.service';
import { forkJoin } from 'rxjs';
import { WeatherInfo } from '../model/weather-report.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  places = [
    {city: 'Mrzeżyno', country: 'PL'},
    {city: 'Grzybowo', country: 'PL'},
    {city: 'Kolobrzeg', country: 'PL'},
    {city: 'Ustronie Morskie', country: 'PL'},
    {city: 'Rewal', country: 'PL'}
  ];
  cityInfo = [];

  constructor(
    private weatherService: WeatherService,
    private router: Router) { }

  ngOnInit() {
    let requestArray = [];
    this.places.map(x => {
      requestArray.push(this.weatherService.getCityDetails(x.city, x.country))
    })
    forkJoin(requestArray).subscribe((response: any) => {
    this.cityInfo = response.map(x => new WeatherInfo(x))
    });
  }

  goToDetails(value) {
    this.router.navigate(['weather-details'], { queryParams: { city: value.name } });
  }

}
