import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { WeatherDetailsComponent } from './weather-details/weather-details.component';
import { HttpClientModule } from '@angular/common/http';
import { TemperaturePipe } from './pipes/temperature.pipe';
import { HpaToMeterPipe } from './pipes/hpa-to-meter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WeatherDetailsComponent,
    TemperaturePipe,
    HpaToMeterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
