import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hpaToMeter'
})
export class HpaToMeterPipe implements PipeTransform {

  transform(value: any): any {
    return (value * 0.010197).toFixed(2);
  }

}
