import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export const config = {
  url: 'http://api.openweathermap.org/',
  apiId: '3d8b309701a13f65b660fa2c64cdc517'
}

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private httpClient: HttpClient) { }


  // getCities(lat, long, cityCount) {
  //   return this.httpClient.get(`${config.url}data/2.5/find?lat=${lat}&lon=${long}&cnt=${cityCount}&appid=${config.apiId}`)
  // }

  getCityDetails(city, country) {
    return this.httpClient.get(`${config.url}data/2.5/weather?q=${city},${country}&appid=${config.apiId}`)
  }

  get5DaysReport(city, count) {
    return this.httpClient.get(`${config.url}data/2.5/forecast?q=${city},PL&cnt=${count}&appid=${config.apiId}`)
  }

}
